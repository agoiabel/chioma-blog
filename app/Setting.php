<?php

namespace App;

use App\Classes\Model\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use ModelHelper;

    /**
     * attr that can be mass assigned
     * @var [type]
     */
    protected $fillable = ['facebook', 'instagram', 'gmail', 'twitter'];
}
