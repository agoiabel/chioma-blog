<?php

namespace App;

use App\Classes\Model\Sluggable;
use App\Classes\Model\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    use ModelHelper, Sluggable;

    /**
     * attr that can be mass assigned
     * 
     * @var [type]
     */
    protected $fillable = ['slug', 'title', 'blog_post', 'user_id', 'category_id', 'numberOfLike', 'seo_helper_content'];

    /**
     * generate slug from
     * 
     * @return 
     */
    public function getSluggableString()
    {
    	return $this->title;
    }

    /**
     * a blog post belongs to a category
     * 
     * @return 
     */
    public function category()
    {
    	return $this->belongsTo(Category::class);	
    }

    /**
     * a blog post has many images
     * 
     * @return 
     */
    public function images()
    {
        return $this->hasMany(BlogPostImage::class);        
    }
    
    /**
     * a blog post has many users
     * 
     * @return 
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
        
    /**
     * a blog post has many comment
     * 
     * @return 
     */
    public function comments()
    {
        return $this->hasMany(BlogPostComment::class, 'blog_post_id');
    }

    /**
     *  ablog post has one page stat
     *  
     * @return 
     */
    public function pagestat()
    {
        return $this->hasOne(PageStat::class, 'blog_post_id');
    }

    /**
     * a blog post comment counts
     * 
     * @return 
     */
    public function getCommentCount()
    {
        return $this->comments()->count();
    }
    
    
}
