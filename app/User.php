<?php

namespace App;

use App\Classes\Model\ModelHelper;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, ModelHelper;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role_id', 'name', 'email', 'password', 'picture', 'description'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * a user belongs to a role
     * 
     * @return 
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * check if user is an admin
     * 
     * @return integer
     */
    public function isAdmin()
    {
        return $this->attributes['role_id'] == Role::ADMIN;
    }

    public function blogposts()
    {
        return $this->hasMany(BlogPost::class);    
    }    

    public function composed_comments()
    {
        return $this->hasMany(BlogPostComment::class, 'composer_id');
    }
}
