<?php

namespace App;

use App\Classes\Model\Sluggable;
use App\Classes\Model\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class BlogPostComment extends Model
{
	
	use ModelHelper, Sluggable;

    /**
     * Generate slug from
     * 
     * @return 
     */
    public function getSluggableString()
    {
        return $this->blogpost->slug . '-' . $this->id;
    }

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['slug', 'blog_post_id', 'composer_id', 'replier_id', 'comment', 'user_status', 'admin_status'];

    const UNREAD = false;
    const READ = true;

    /**
     * a comment belongs to a blog post
     * 
     * @return 
     */
    public function blogpost()
    {
    	return $this->belongsTo(BlogPost::class, 'blog_post_id');
    }
    
    public function composer()
    {
    	return $this->belongsTo(User::class, 'composer_id');
    }

    public function replyer()
    {
        return $this->belongsTo(User::class, 'replier_id');
    }

}
