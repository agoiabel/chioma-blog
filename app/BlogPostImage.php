<?php

namespace App;

use App\Classes\Model\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class BlogPostImage extends Model
{
    use ModelHelper;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['blog_post_id', 'image', 'full_image'];

    public function blog_posts()
    {
    	return $this->belongsTo(BlogPost::class);
    }
}
