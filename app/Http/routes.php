<?php

Route::group(['prefix' => 'api'], function () {
	get('blog/list', ['as' => 'blog.list', 'uses' => 'BlogController@listBlog']);
	get('category/list', ['as' => 'category.list', 'uses' => 'CategoryController@listCategory']);
	get('category/blog-list/{slug}', ['as' => 'blog.list', 'uses' => 'CategoryController@listBlog']);
	get('blog/destroy/{id}', ['middleware' => 'auth', 'as' => 'blog.destroy', 'uses' => 'BlogController@destroy']);
	get('category/destroy/{id}', ['middleware' => 'auth', 'as' => 'category.destroy', 'uses' => 'CategoryController@destroy']);
	
	get('blog/update-like/{id}', ['as' => 'blog.update-like', 'uses' => 'BlogController@updateLike']);
	post('blog/content-image-upload', ['as' => 'blog.content-image-upload', 'uses' => 'BlogController@postBlogContentImage']);
});

Route::group(['middleware' => 'auth'], function () {
	get('blog/index', ['as' => 'blog.index', 'uses' => 'BlogController@index']);
	get('blog/show', ['as' => 'blog.show', 'uses' => 'BlogController@show']);
	get('blog/create', ['as' => 'blog.create', 'uses' => 'BlogController@create']);
	post('blog/create', ['as' => 'blog.store', 'uses' => 'BlogController@store']);
	get('blog/edit/{slug}', ['as' => 'blog.edit', 'uses' => 'BlogController@edit']);
	post('blog/edit/{slug', ['as' => 'blog.update', 'uses' => 'BlogController@update']);

	// get('blog/create/image/{slug}', ['as' => 'blog.create-image', 'uses' => 'BlogController@createImage']);	
	// post('blog/create/image', ['as' => 'blog.post-image', 'uses' => 'BlogController@postImage']);

	// get('blog/edit-image/{slug}', ['as' => 'blog-edit.image', 'uses' => 'BlogController@editImage']);


	get('category/index', ['as' => 'category.index', 'uses' => 'CategoryController@index']);
	get('category/create', ['as' => 'category.create', 'uses' => 'CategoryController@create']);
	post('category/create', ['as' => 'category.store', 'uses' => 'CategoryController@store']);

	get('setting', ['as' => 'setting', 'uses' => 'SettingController@index']);
	post('setting', ['as' => 'setting.store', 'uses' => 'SettingController@store']);

	post('password/reset', ['as' => 'password.reset', 'uses' => 'SettingController@password']);
	post('password/update', ['as' => 'profile.update', 'uses' => 'SettingController@update']);
	get('dashboard', ['middleware' => 'is_admin', 'as' => 'dashboard', 'uses' => 'GuestController@backend']);

});

get('/login', ['as' => 'login', 'uses' => 'AuthenticationController@login']);
post('/login', ['as' => 'post.login', 'uses' => 'AuthenticationController@postLogin']);
get('/logout', ['as' => 'logout', 'uses' => 'AuthenticationController@logout']);

get('/', ['as' => 'index', 'uses' => 'GuestController@index']);
get('/category/{slug}', ['as' => 'category.show', 'uses' => 'GuestController@category']);
get('/single-blog/{slug}', ['as' => 'single-blog', 'uses' => 'GuestController@single']);

post('comment/store', ['as' => 'comment.store', 'uses' => 'CommentController@store']);
get('comment/index', ['as' => 'comment.index', 'uses' => 'CommentController@index']);
get('comment/delete/{id}', ['as' => 'comment.delete', 'uses' => 'CommentController@delete']);
get('comment/show/{slug}', ['as' => 'comment.show', 'uses' => 'CommentController@show']);
