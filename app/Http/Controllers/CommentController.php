<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\BlogPostComment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentFormRequest;

class CommentController extends Controller
{
	/**
	 * process the storing of comment
	 * 
	 * @param  CommentFormRequest $request 
	 * @return 
	 */
    public function store(CommentFormRequest $request)
    {
    	$request->handle();

    	if ($request->ajax()) {
    		return response()->json('your comment was created successfully');
    	}

    	return redirect()->back();
    }

    /**
     * diplay view to show all user,s comments
     * 
     * @return 
     */
    public function index(BlogPostComment $comment)
    {
    	$comments = $comment->where('replier_id', \Auth::user()->id)->latest()->paginate(10);

    	return view('blog.comment', compact('comments'));
    }

    /**
     * delete comment instance
     * @param            $id      
     * @param  BlogPostComment $comment 
     * @return                    
     */
    public function delete($id, BlogPostComment $comment)
    {
    	$comment->where('id', $id)->firstOrFail()->delete();

    	flash()->success('success', 'comment was deleted successfully');

    	return redirect()->back();
    }

    /**
     * find comment
     * 
     * @param  $id      
     * @param  BlogPostComment $comment 
     * @return                    
     */
    public function show($id, BlogPostComment $comment)
    {
        return response()->json($comment->with(['blogpost'])->where('id', $id)->firstOrFail());
    }

}
