<?php

namespace App\Http\Controllers;

use App\BlogPost;
use App\Category;
use App\Http\Requests;
use App\BlogPostImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\Utility\FileUploader;
use App\Http\Requests\BlogFormRequest;
use App\Http\Requests\BlogUpdateFormRequest;
// use App\Http\Requests\BlogImageFormRequest;
use App\Http\Requests\BlogContentFormRequest;

class BlogController extends Controller
{
    /**
     * display view to show all blog
     * 
     * @return 
     */
    public function index()
    {
        return view('blog.index');
    }

    /**
     * display view to create blog
     * 
     * @param  Category $category 
     * @return 
     */
    public function create(Category $category)
    {
        $categories = $category->getAll();

        return view('blog.create', compact('categories'));
    }

    /**
     * process the storing of blogs
     * 
     * @param  BlogFormRequest $request 
     * @return 
     */
    public function store(BlogFormRequest $request)
    {
        $request->handle();

        flash()->success('success', 'your content was uploaded successfully');

        return redirect()->route('blog.index');
    }

    /**
     * display view to edit blog
     * @param  $slug 
     * @param  BlogPost $blog 
     * @return          
     */
    public function edit($slug, BlogPost $blog, Category $category)
    {
        $blog = $blog->where('slug', $slug)->firstOrFail();
        $categories = $category->lists('name', 'id');

        return view('blog.edit', compact('blog', 'categories'));
    }

    /**
     * update blog content
     * 
     * @param  BlogUpdateFormRequest $request 
     * @return 
     */
    public function update(BlogUpdateFormRequest $request)
    {
        $request->handle();

        flash()->success('success', 'your content was updated successfully');

        return redirect()->route('blog.index');
    }

    /**
     * delete blog
     * 
     * @param  $id   
     * @param  BlogPost $blog 
     * @return 
     */
    public function destroy($id, BlogPost $blog)
    {
        $blog->where('id', $id)->firstOrFail()->delete();   
        return;
    }

    /**
     * handle storing of individual uploaded images from summernote editor
     * @param  BlogContentFormRequest $request 
     * @return image_path
     */
    public function postBlogContentImage(BlogContentFormRequest $request)
    {
        return $request->handle();
    }

    /**
     * return list of blogs
     * 
     * @return 
     */
    public function listBlog(BlogPost $blogPost)
    {
        return response()->json($blogPost->with(['category','images', 'comments', 'pagestat' => function ($query) {
            $query->latest();
        }])->latest()->paginate(8));
    }


    /**
     * update blog count
     * @param  Request  $request 
     * @param  BlogPost $blog    
     * @param     $id      
     * @return 
     */
    public function updateLike(Request $request, BlogPost $blog, $id)
    {
        $blog = $blog->findBy('id', $id);

        $blog->updateModel([
            'numberOfLike' => $blog->numberOfLike + 1
        ]);

        if ($request->ajax()) {
            return response()->json('you successfully like post');
        }
        return redirect()->back();
    }


    // /**
    //  * display edit form
    //  * 
    //  * @param     $slug 
    //  * @param  BlogPost $blog 
    //  * @return          
    //  */
    // public function editImage($slug, BlogPost $blog)
    // {
    //     $blog = $blog->findBy('slug', $slug);

    //     return view('blog.edit-image', compact('blog'));
    // }

    // /**
    //  * display view to upload blog images
    //  * 
    //  * @return 
    //  */
    // public function createImage($slug, BlogPost $blog)
    // {
    //     $blog = $blog->findBy('slug', $slug);

    //     return view('blog.createImage', compact('blog'));   
    // }

    // /**
    //  * process the storing of blog images
    //  * 
    //  * @param  BlogImageFormRequest $request 
    //  * @return 
    //  */
    // public function postImage(BlogImageFormRequest $request)
    // {
    //     $request->handle();

    //     if ($request->upload_another == "no") {
    //         flash()->success('success', 'your blog content was successfully uploaded');
    //         return redirect()->route('blog.index');
    //     }
    //     flash()->success('success', 'your image was uploaded successfully, you can upload another one');
    //     return redirect()->back();
    // }
    
}
