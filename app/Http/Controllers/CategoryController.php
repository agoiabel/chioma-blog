<?php

namespace App\Http\Controllers;

use App\BlogPost;
use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryFormRequest;

class CategoryController extends Controller
{
	/**
	 * return list of categories
	 * 
	 * @return 
	 */
    public function listBlog(Category $category, BlogPost $blog, $slug)
    {
		$category_id = $category->where('slug', $slug)->firstOrFail()->id;

		$blogs = $blog->with(['images'])->where('category_id', $category_id)->paginate(8);
    	
    	return response()->json($blogs);
    }


    public function index()
    {
    	return view('category.index');
    }


    /**
     * display view to show all categories
     * 
     * @return 
     */
    public function listCategory(Category $category)
    {
    	return response()->json($category->paginate(10));
    }


    public function destroy($id, Category $category)
    {
        $category->where('id', $id)->firstOrFail()->delete();   
        return;
    }

    /**
     * display view to show category
     * 
     * @return 
     */
    public function create()
    {
    	return view('category.create');	
    }


    public function store(CategoryFormRequest $request)
    {
    	$request->handle();

    	flash()->success('suucess', 'category was created successfully');

    	return redirect()->back();
    }

}
