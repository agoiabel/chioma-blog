<?php

namespace App\Http\Controllers;

use App\Category;
use App\BlogPost;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GuestController extends Controller
{
	/**
	 * display view to show index page
	 * @param  Category $category 
	 * @return             
	 */
    public function index(Category $category, BlogPost $blog)
    {
        $blogs = $blog->orderByRaw("RAND()")->limit(3)->get();
    	$categories = $category->getAll();

    	return view('guest.index', compact('categories', 'blogs'));
    }

    public function category(Category $category, $slug)
    {
        $category = $category->findBy('slug', $slug);

        return view('guest.category', compact('category'));
    }

    public function single($slug, BlogPost $blog)
    {
        try {
            $blog = $blog->findBy('slug', $slug);

            $blog->pagestat->updateModel([
                'no_of_view' => $blog->pagestat->no_of_view + 1
            ]);
            
            try {
                $previous_slug = $blog->where('id', $blog->where('id', '<', $blog->id)->max('id'))->firstOrFail()->slug;
            } catch (ModelNotFoundException $e) {
                $previous_slug = null;
            }            

            try {
                $next_slug = $blog->where('id', $blog->where('id', '>', $blog->id)->min('id'))->firstOrFail()->slug;                
            } catch (ModelNotFoundException $e) {
                $next_slug = null;
            }            

        	return view('guest.single', compact('blog', 'previous_slug', 'next_slug'));
            
        } catch (ModelNotFoundException $e) {
            return redirect()->back();
        }
    }
    
    public function backend()
    {
        return view('dashboard');
    }
    
}
