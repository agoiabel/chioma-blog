<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;

class AuthenticationController extends Controller
{
	/**
	 * display view to show login
	 * 
	 * @return 
	 */
    public function login()
    {
    	return view('guest.login');
    }

    public function postLogin(LoginFormRequest $request)
    {
    	$request->handle();

    	return redirect()->route('blog.index');
    }

    public function logout(Guard $auth)
    {
        $auth->logout();

        return redirect()->route('login');
    }

}
