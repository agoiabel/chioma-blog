<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SettingFormRequest;
use App\Http\Requests\ProfileFormRequest;
use App\Http\Requests\PasswordFormRequest;

class SettingController extends Controller
{
	/**
	 * display view for settings
	 * 
	 * @return 
	 */
    public function index()
    {
    	return view('setting.index');
    }

    public function password(PasswordFormRequest $request)
    {
    	$request->handle();

    	flash()->success('success', 'password was updated successfully');

    	return redirect()->back();
    }

    public function update(ProfileFormRequest $request)
    {
    	$request->handle();

    	flash()->success('success', 'profile was updated successfully');

    	return redirect()->back();
    }

    public function store(SettingFormRequest $request)
    {
        $request->handle();

        flash()->success('success', 'profile was updated successfully');

        return redirect()->back();
    }
}
