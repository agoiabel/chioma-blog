<?php

namespace App\Http\Requests;

use App\User;
use App\Role;
use App\BlogPost;
use App\BlogPostComment;
use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CommentFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => 'required',
            'name' => 'required',
            'email' => 'required|email',
        ];
    }

    /**
     * handle processing of comment
     * 
     * @return 
     */
    public function handle()
    {
        try {
            $user_id = (new User())->findBy('email', $this->email)->id;
        } catch (ModelNotFoundException $e) {
            $user_id = (new User())->initModel([
                'role_id' => Role::OTHER,
                'email' => $this->email,
                'name' => $this->name,
                'picture' => 'img/default-profile.png',
            ])->saveModel()->id;
        }

        $replier_id = (new BlogPost())->where('id', $this->blog_post_id)->firstOrFail()->user_id;

        (new BlogPostComment())->initModel([
            'blog_post_id' => $this->blog_post_id,
            'composer_id' => $user_id,
            'replier_id' => ( $replier_id == $user_id) ? '' : $replier_id,
            'comment' => $this->comment,
        ])->saveModel();

        if (! is_null($this->comment_id)) {
            (new BlogPostComment())->findBy('id', $this->comment_id)->updateModel([
                'user_status' => BlogPostComment::READ
            ])->saveModel();
        }
    }

}
