<?php

namespace App\Http\Requests;

use App\User;
use App\Http\Requests\Request;

class PasswordFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ];
    }

    public function handle()
    {
        (new User())->where('id', $this->user_id)->firstOrFail()->updateModel([
            'password' => $this->password
        ]);
    }
}
