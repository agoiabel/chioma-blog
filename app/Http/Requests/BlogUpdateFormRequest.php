<?php

namespace App\Http\Requests;

use App\BlogPost;
use App\Http\Requests\Request;
use App\Http\Requests\BlogRequest;
use App\Classes\Utility\BlogHelper;

class BlogUpdateFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return (new BlogRequest())->rules();
    }

    public function handle()
    {
        $blog = (new BlogPost())->findBy('id', $this->blog_id)->updateModel([
            'title' => $this->title,
            'category_id' => $this->category_id,
            'blog_post' => $this->blog_post,
        ]);

        foreach ($blog->images as $key => $image) {
            $image->delete();
        }
  
        (new BlogHelper())->syncBlogImage($this->blog_post, $blog->id);

        return $blog;
    }

}
