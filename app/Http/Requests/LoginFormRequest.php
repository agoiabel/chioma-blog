<?php

namespace App\Http\Requests;

use App\User;
use App\Http\Requests\Request;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LoginFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
        ];
    }

    public function handle()
    {
        $this->checkUserCredential();    
    }

    public function checkUserCredential()
    {
        try {
            $user = (new User())->where('email', $this->email)->firstOrFail();
            if (! Hash::check($this->password, $user->password) )
            {
                throw new CustomException(route('login'), 'credential is invalid');
            }
            
            \Auth::login($user);

            return;

        } catch (ModelNotFoundException $e) {
            throw new CustomException(route('login'), 'credential is invalid');
        }
    }

}
