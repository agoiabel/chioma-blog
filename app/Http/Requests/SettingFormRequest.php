<?php

namespace App\Http\Requests;

use App\Setting;
use App\Http\Requests\Request;

class SettingFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'facebook' => 'required_without_all:twitter,instagram,gmail',
            'twitter' => 'required_without_all:facebook,instagram,gmail',
            'instagram' => 'required_without_all:twitter,facebook,gmail',
            'gmail' => 'required_without_all:twitter,instagram,facebook',
        ];
    }

    /**
     * process handling of setting
     * 
     * @return 
     */
    public function handle()
    {
        (new Setting())->where('id', 1)->firstOrFail()->updateModel([
            'facebook' => $this->facebook,
            'twitter' => $this->twitter,
            'instagram' => $this->instagram,
            'gmail' => $this->gmail,
        ]);
    }
}
