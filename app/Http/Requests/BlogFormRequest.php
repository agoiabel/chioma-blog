<?php

namespace App\Http\Requests;

use App\BlogPost;
use App\PageStat;
use App\BlogPostImage;
use App\Http\Requests\Request;
use App\Http\Requests\BlogRequest;
use App\Classes\Utility\BlogHelper;

class BlogFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return (new BlogRequest())->rules();
    }

    public function handle()
    {   
        $blog = (new BlogPost())->initModel($this->all())->saveModel();
        
        (new PageStat())->initModel(['blog_post_id' => $blog->id])->saveModel();
        
        (new BlogHelper())->syncBlogImage($this->blog_post, $blog->id);

        return $blog;
    }
    
}
