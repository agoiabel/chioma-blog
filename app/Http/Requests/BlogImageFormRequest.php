<?php

namespace App\Http\Requests;

use App\BlogPostImage;
use App\Http\Requests\Request;
use App\Classes\Utility\FileUploader;

class BlogImageFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'caption' => 'required',
            'upload_another' => 'required',
            'photos' => 'required'
        ];

        $photos = $this->file('photos');

        if ( !empty($photos) )
        {
            foreach ( $photos as $key => $photo ) // add individual rules to each image
            {
                $rules[ sprintf( 'photos.%d', $key ) ] = 'required|image';
            }
        }
        return $rules;
    }

    public function handle()
    {
        $image = $this->file('photos')[0];        
        $file_path = (new FileUploader())->uploadFile($image);

        (new BlogPostImage())->initModel([
            'blog_post_id' => $this->blog_post_id,
            'image' => $file_path,
            'full_image' => $file_path,
        ])->saveModel();        

    }

}
