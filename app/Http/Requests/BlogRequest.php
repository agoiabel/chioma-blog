<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'title' => 'required',
            'category_id' => 'required',
            'blog_post' => 'required',            
            'seo_helper_content' => 'required',
        ];
    }
}
