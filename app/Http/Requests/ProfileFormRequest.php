<?php

namespace App\Http\Requests;

use App\User;
use App\Http\Requests\Request;
use App\Classes\Utility\FileUploader;

class ProfileFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'picture' => 'required|mimes:png,jpeg,jpg',
            'description' => 'required',
        ];
    }

    public function handle()
    {
        (new User())->where('id', $this->user_id)->firstOrFail()->updateModel([
            'name' => $this->name,
            'email' => $this->email,
            'picture' => (new FileUploader())->makeThumbnail($this->picture, 40, 40),
            'description' => $this->description,
        ]);    
    }

}
