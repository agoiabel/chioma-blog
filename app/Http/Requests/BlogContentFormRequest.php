<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Classes\Utility\FileUploader;

class BlogContentFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function handle()
    {
        return  '/'.(new FileUploader())->uploadFile($this->file('file'));
    }

}
