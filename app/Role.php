<?php

namespace App;

use App\Classes\Model\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use ModelHelper;

    /**
     * attr that can be mass assigned
     * @var array
     */
    protected $fillable = ['name'];

    const ADMIN = '1';
    const MODERATOR = '2';
    const OTHER = '3';

    /**
     * a role has many users
     * 
     * @return 
     */
    public function user()
    {
    	return $this->hasMany(User::class);
    }

}
