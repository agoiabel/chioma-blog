<?php

namespace App;

use App\Classes\Model\Sluggable;
use App\Classes\Model\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use ModelHelper, Sluggable;

    /**
     * attr that can be mass assigned
     * 
     * @var [type]
     */
    protected $fillable = ['slug', 'name'];

    /**
     * generate slug from
     * 
     * @return 
     */
    public function getSluggableString()
    {
    	return $this->name;
    }

    public function blogposts()
    {
    	return $this->hasMany(BlogPost::class);
    }

}
