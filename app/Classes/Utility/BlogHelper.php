<?php 

namespace App\Classes\Utility;

use App\BlogPostImage;

class BlogHelper
{
    /**
     * save blog image
     * @param   $image_srcs 
     * @param   $blog_id    
     * @return              
     */
    public function syncBlogImage($blog_post_content, $blog_id)
    {
        foreach ($this->getImageFrom($blog_post_content) as $key => $image_src) {
            (new BlogPostImage())->initModel([
                'blog_post_id' => $blog_id,
                'image' => $image_src,
                'full_image' => $image_src
            ])->saveModel();            
        }
    }

    /**
     * get image source
     * 
     * @param $html_string 
     * @return 
     */
    protected function getImageFrom($html_string)
    {
        preg_match_all('/<img[^>]+>/i', $html_string, $image_tags);
        for ($i = 0; $i < count($image_tags[0]); $i++) {
            preg_match('/src="([^"]+)/i', $image_tags[0][$i], $image);

            $originalImageSrc[] = str_ireplace('src="/', '', $image[0]);
        }
        return $originalImageSrc;
    }

}