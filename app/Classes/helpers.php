<?php 

use App\User;
use App\Setting;
use App\BlogPostComment;

function flash($title = null, $message = null)
{
    $flash = app('App\Classes\Utility\Flash');

    if (func_num_args() == 0) {
    	return $flash;
    }

    return $flash->info($title, $message);
}

function words($value, $words = 7, $end = '...')
{
    return \Illuminate\Support\Str::limit($value, $words, $end);
}


function comment_read_status($status) 
{
	if ($status == BlogPostComment::UNREAD) {
		return 'not read';
	}
	return 'read';
}


function isActiveRoute($route, $output = "active")
{
    if (Route::currentRouteName() == $route) return $output;
}

function areActiveRoutes(Array $routes, $output = "active")
{
    foreach ($routes as $route)
    {
        if (Route::currentRouteName() == $route) return $output;
    }

}

function getSetting($value) {
    return (new Setting())->findBy('id', 1)->$value;
}