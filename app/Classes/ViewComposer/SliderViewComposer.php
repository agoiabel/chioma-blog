<?php 

namespace App\Classes\ViewComposer;

use App\BlogPost;
use Illuminate\Contracts\View\View;

class SliderViewComposer {

    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $blog;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(BlogPost $blog)
    {
        $this->blog = $blog;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('blogs', $this->blog->getPaginated(5) );
    }
    
}