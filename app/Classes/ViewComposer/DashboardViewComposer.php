<?php 

namespace App\Classes\ViewComposer;

use App\Category;
use App\BlogPost;
use App\PageStat;
use Carbon\Carbon;
use App\BlogPostComment;
use Illuminate\Contracts\View\View;

class DashboardViewComposer {

    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $comment;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(BlogPost $blog, BlogPostComment $comment, Category $category)
    {
        $this->blog = $blog;
        $this->comment = $comment;
        $this->category = $category;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('total_blog_post', $this->blog->getAll() );
        $view->with('total_page_stat', $this->getPageStat() );
        $view->with('total_page_stat_for_today', $this->getPageStatForToday() );
        $view->with('total_comments', $this->comment->getAll() );
        $view->with('total_category', $this->category->getAll() );
        $view->with('month_blog_stat', $this->getMonthBlogStat() );

        $view->with('week_blog_stat_date', $this->getWeekBlogStatDate() );
        $view->with('week_blog_stat_value', $this->getWeekBlogStatValue() );

        $view->with('month_page_stat', $this->getMonthPageViewStat() );
    }


    public function getWeekBlogStatDate()
    {        
        $chartDatas = BlogPost::select([
            \DB::raw('DATE(created_at) AS date'),
            \DB::raw('COUNT(id) AS count'),
         ])
         ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
         ->groupBy('date')
         ->orderBy('date', 'ASC')
         ->get()
         ->toArray();

        $chartDataByDay = array();
        $chartDataDate = [];

        foreach($chartDatas as $data) {
            $chartDataByDay[$data['date']] = $data['count'];
        }

        $date = new Carbon;
        for($i = 0; $i < 30; $i++) {
            $dateString = $date->format('Y-m-d');
            
            if(! isset($chartDataByDay[ $dateString ] )) {
                $chartDataByDay[ $dateString ] = 0;
            }

            $date->subDay();
        }
        $result = [];

        foreach ($chartDataByDay as $key => $value) {
            $chartDataByDate[] = $key;
        }
        return $chartDataByDate;
    }


    public function getWeekBlogStatValue()
    {        
        $chartDatas = BlogPost::select([
            \DB::raw('DATE(created_at) AS date'),
            \DB::raw('COUNT(id) AS count'),
         ])
         ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
         ->groupBy('date')
         ->orderBy('date', 'ASC')
         ->get()
         ->toArray();

        $chartDataByDay = array();
        $chartDataDate = [];

        foreach($chartDatas as $data) {
            $chartDataByDay[$data['date']] = $data['count'];
        }

        $date = new Carbon;
        for($i = 0; $i < 30; $i++) {
            $dateString = $date->format('Y-m-d');
            
            if(! isset($chartDataByDay[ $dateString ] )) {
                $chartDataByDay[ $dateString ] = 0;
            }

            $date->subDay();
        }

        foreach ($chartDataByDay as $key => $value) {
            $chartDataByValue[] = $value;
        }
        return $chartDataByValue;
    }

    public function getMonthBlogStat()
    {
        $mons = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $months = [];
        foreach ($mons as $month) {
            $months[] = BlogPost::whereRaw('MONTH(created_at) = ?',[$month])->count();
        }
        return $months;
    }

    public function getMonthPageViewStat()
    {
        $mons = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $months = [];
        foreach ($mons as $month) {
            $stats = PageStat::whereRaw('MONTH(created_at) = ?',[$month])->get();

            $no_of_view = 0;
            foreach ($stats as $key => $value) {
                $no_of_view += $value->no_of_view;
            }
            $months[] = $no_of_view;
        }
        return $months;   
    }

    public function getPageStat()
    {
        $total_page_stat = 0;
        foreach ( (new PageStat())->getAll() as $key => $page ) {
            $total_page_stat += $page->no_of_view;
        }
        return $total_page_stat;
    }

    public function getPageStatForToday()
    {
        $total_page_stat = 0;
        foreach ( (new PageStat())->where('created_at', '>=', Carbon::now())->get() as $key => $page ) {
            $total_page_stat += $page->no_of_view;
        }
        return $total_page_stat;   
    }
}