<?php 

namespace App\Classes\ViewComposer;

use App\BlogPostComment;
use Illuminate\Contracts\View\View;

class BackendHeaderViewComposer {

    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $comment;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(BlogPostComment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('comments', $this->comment->where('replier_id', \Auth::user()->id)->where('user_status', BlogPostComment::UNREAD)->latest()->paginate(5) );
    }

}