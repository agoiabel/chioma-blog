<?php 

namespace App\Classes\Model;

trait ModelHelper {
    /**
     * save new 
     * 
     * @return 
     */
    public function saveModel()
    {
        $this->save();

        return $this;
    }

    /**
     * initialize type
     * 
     * @return 
     */
    public static function initModel($params)
    {
        $array = [];

        foreach ($params as $key => $value) {
            $array[$key] = $value;
        }

        return new static($array);
    }

    /**
     * update model
     * 
     * @param     
     * @param  [] $params 
     * @return          
     */
    public function updateModel($params)
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        return $this->saveModel();
    }

    /**
     * Find By Column and column attr
     * @param   $column 
     * @param   $attr   
     * @return          
     */
    public function findBy($column, $attr)
    {
        return $this->where($column, $attr)->firstOrFail();
    }

    /**
     * Get All
     * 
     * @return 
     */
    public function getAll()
    {
        return $this->latest()->get();
        // return $this->latest()->whereNull('deleted_at')->get();
    }

    /**
     * Get all alphabetically
     * 
     * @return 
     */
    public function getAllAlphabeticallyFor($attr)
    {
        return $this->orderby($attr, 'ASC')->get();
    }

    /**
     * Get paginated
     * 
     * @return 
     */
    public function getPaginated($value)
    {
        return $this->latest()->paginate($value);
    }
    
}