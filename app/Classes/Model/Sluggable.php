<?php 

namespace App\Classes\Model;

trait Sluggable
{
    public static function bootSluggable()
    {
        static::created(function ($model) {
                $slug = $model->getSluggableString();

                $make_slug = $model->id . '-' . $slug;

                $model->slug = str_slug($make_slug);

                $model->save();
            });

        static::updating(function ($model) {
                $slug = $model->getSluggableString();
                
                $make_slug = $model->id . '-' . $slug;

                $model->slug = str_slug($make_slug);
            });
    }
}