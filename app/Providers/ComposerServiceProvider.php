<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('shared._mobile_css', 'App\Classes\ViewComposer\MobileHeaderViewComposer');
        view()->composer('shared._header', 'App\Classes\ViewComposer\HeaderViewComposer');
        view()->composer('guest._sidebar', 'App\Classes\ViewComposer\SidebarViewComposer');
        view()->composer('shared._backend_header_nav', 'App\Classes\ViewComposer\BackendHeaderViewComposer');
        view()->composer('dashboard', 'App\Classes\ViewComposer\DashboardViewComposer');
        view()->composer('guest._index_slider', 'App\Classes\ViewComposer\SliderViewComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
