<?php

namespace App;

use App\Classes\Model\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class PageStat extends Model
{
    use ModelHelper;

    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['blog_post_id', 'no_of_view'];

    /**
     * a page stat belongs to blogpost
     * @return 
     */
    public function blogpost()
    {
		return $this->belongsTo(BlogPost::class);    	
    }


}
