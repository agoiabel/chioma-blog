var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');

    /**
     * frontend style and scripts
     */
    mix.styles([
        'bootstrap.min.css',
        'sweetalert.css',
        'ionicons.min.css',
        'custom-style.css',
        'responsive.css'
    ], 'public/css/front-style.css', 'public/css');

    mix.scripts([
        'jquery-3.1.1.min.js',
        'libs/sweetalert.min.js',
        'owl.carousel.min.js',
        'instafeed.js',
        'custom-script.js',
        'waves-script.js',
        'theiaStickySidebar.js',
    ], 'public/js/front-script.js', 'public/js');

    /**
     * backend style and scripts
     */
    mix.styles([

        'plugins/pace-master/themes/blue/pace-theme-flash.css',
        'plugins/uniform/css/uniform.default.min.css',        
        'plugins/bootstrap/css/bootstrap.min.css',
        'plugins/fontawesome/css/font-awesome.css',        
        'plugins/line-icons/simple-line-icons.css',

        'plugins/offcanvasmenueffects/css/menu_cornerbox.css',
        
        'plugins/waves/waves.min.css',
        'plugins/3d-bold-navigation/css/style.css',
        'plugins/slidepushmenus/css/component.css',
        'plugins/weather-icons-master/css/weather-icons.min.css',
        
        'plugins/metrojs/MetroJs.min.css',
        'plugins/toastr/toastr.min.css',

        'modern.min.css',
        'themes/green.css',
        'custom.css',

        'sweetalert.css',
    ], 'public/css/backend-style.css', 'public/css');

    mix.scripts([

        'plugins/jquery/jquery-2.1.4.min.js',
        'plugins/jquery-ui/jquery-ui.min.js',
        'plugins/pace-master/pace.min.js',
        'plugins/jquery-blockui/jquery.blockui.js',

        'plugins/bootstrap/js/bootstrap.min.js',
        'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'plugins/uniform/jquery.uniform.min.js',

        'plugins/offcanvasmenueffects/js/classie.js',
        'plugins/offcanvasmenueffects/js/main.js',
        'plugins/waypoints/jquery.waypoints.min.js',

        'plugins/jquery-counterup/jquery.counterup.min.js',
        'plugins/toastr/toastr.min.js',
        'plugins/flot/jquery.flot.min.js',

        'plugins/flot/jquery.flot.time.min.js',
        'plugins/flot/jquery.flot.symbol.min.js',
        'plugins/flot/jquery.flot.resize.min.js',

        'plugins/flot/jquery.flot.tooltip.min.js',
        'plugins/curvedlines/curvedLines.js',
        'plugins/metrojs/MetroJs.min.js',
        'modern.js',
        'libs/sweetalert.min.js'


    ], 'public/js/backend-script.js', 'public/js');

});
