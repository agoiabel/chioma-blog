<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_post_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('composer_id')->unsigned();
            $table->integer('replier_id')->unsigned();
            $table->integer('blog_post_id')->unsigned();
            $table->string('slug');

            $table->string('comment');

            $table->boolean('user_status')->default(false);
            $table->boolean('admin_status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_post_comments');
    }
}
