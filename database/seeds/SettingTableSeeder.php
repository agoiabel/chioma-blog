<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new Setting())->initModel([
            'facebook' => 'www.facebook.com'
        ])->saveModel();
    }
}
