<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role_id' => Role::ADMIN,
            'name' => 'abel agoi',
            'email' => 'agoiabeladeyemi@gmail.com',
            'password' => 'abc'
        ]);

        User::create([
            'role_id' => Role::ADMIN,
        	'name' => 'admin',
        	'email' => 'info@themoralwoman.com',
        	'password' => 'themoralwoman'
        ]);
    }
}
