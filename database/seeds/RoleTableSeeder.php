<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$roles = [
			['name' => 'admin'],
			['name' => 'moderator'],
			['name' => 'other'],
		];        

		foreach ($roles as $key => $category) {
			Role::create($category);
		}

    }
}
