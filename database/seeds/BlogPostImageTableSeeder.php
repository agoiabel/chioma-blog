<?php

use App\BlogPost;
use App\BlogPostImage;
use Illuminate\Database\Seeder;

class BlogPostImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blogs = (new BlogPost())->getAll();

        foreach ($blogs as $key => $blog) {

        	foreach (range(1, 3) as $key => $value) {
	        	BlogPostImage::create([
	        		'blog_post_id' => $blog->id,
	        		'image' => 'img/blog/featured-image.jpg',
	        		'full_image' => 'img/blog/featured-image-full.jpg',
	        	]);
        	}
        	
        }
    }
}
