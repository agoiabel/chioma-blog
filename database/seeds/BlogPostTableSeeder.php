<?php

use App\PageStat;
use App\BlogPost;
use App\Category;
use Illuminate\Database\Seeder;

class BlogPostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();

		$categories = (new Category())->getAll();

		foreach ($categories as $category)
		{
			foreach (range(1, 12) as $key => $value) {
				$blog = BlogPost::create([
					'user_id' => 1,
					'category_id' => $category->id,
					'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
					'blog_post' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true)
				]);

				(new PageStat())->initModel([
					'blog_post_id' => $blog->id
				])->saveModel();
			}
		}
		
    }
}
