<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$categories = [
			['name' => 'fashion'],
			['name' => 'lifestyle'],
			['name' => 'Music'],
		];        

		foreach ($categories as $key => $category) {
			Category::create($category);
		}

    }
}
