<div class="tw-mobile-menu"><i class="ion-ios-close-empty"></i>
    <nav>
        <ul class="sf-mobile-menu clearfix">
            <li class="menu-item current-menu-item menu-item-has-children">
                <a href="index.html">Home</a>
                <ul class="sub-menu">
                    <li class="menu-item">
                        <a href="index.html">Home Style 1</a>
                        <ul class="sub-menu">
                            <li class="menu-item">
                                <a href="home-1-full-width.html">Full Width</a>
                            </li>
                            <li class="menu-item">
                                <a href="home-1-no-sidebar.html">No Sidebar</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item">
                        <a href="home-2.html">Home Style 2</a>
                        <ul class="sub-menu">
                            <li class="menu-item">
                                <a href="home-2-no-sidebar.html">No Sidebar</a>
                            </li>
                            <li class="menu-item">
                                <a href="home-2-full-width.html">Fullwidth</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item">
                        <a href="home-3.html">Home Style 3</a>
                        <ul class="sub-menu">
                            <li class="menu-item">
                                <a href="home-3-no-sidebar.html">No Sidebar</a>
                            </li>
                            <li class="menu-item">
                                <a href="home-3-full-width.html">Full Width</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item">
                        <a href="home-4.html">Home Style 4</a>
                        <ul class="sub-menu">
                            <li class="menu-item">
                                <a href="home-4-no-sidebar.html">No Sidebar</a>
                            </li>
                            <li class="menu-item">
                                <a href="home-4-full-width.html">Full Width</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item">
                        <a href="home-5.html">Home Style 5</a>
                        <ul class="sub-menu">
                            <li class="menu-item">
                                <a href="home-5-no-sidebar.html">No Sidebar</a>
                            </li>
                            <li class="menu-item">
                                <a href="home-5-full-width.html">Full Width</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </li>
            <li class="menu-item menu-item-has-children">
                    <a href="#">Features</a>
                <ul class="sub-menu">
                    <li class="menu-item"><a href="blog-single.html">Standard Post</a></li>
                    <li class="menu-item"><a href="blog-single-fullwidth.html">Full Width Post</a></li>
                    <li class="menu-item"><a href="blog-single-music.html">Music Post</a></li>
                    <li class="menu-item"><a href="blog-single-gallery.html">Gallery Post</a></li>
                    <li class="menu-item"><a href="page-typography.html">Typography</a></li>
                    <li class="menu-item"><a href="blog-single-video.html">Video Post</a></li>
                    <li class="menu-item"><a href="page-404.html">404</a></li>
                </ul>
            </li>
            <li class="menu-item menu-item-has-children">
                    <a href="#">Categories</a>
                <ul class="sub-menu">
                    <li class="menu-item"><a href="page-category.html">Fashion</a></li>
                    <li class="menu-item"><a href="page-category.html">Lifestyle</a></li>
                    <li class="menu-item"><a href="page-category.html">Music</a></li>
                    <li class="menu-item"><a href="page-category.html">Travel</a></li>
                </ul>
            </li>
            <li class="menu-item"><a href="page-about.html">About</a></li>
            <li class="menu-item"><a href="page-contact.html">Contact</a></li>
        </ul>
    </nav>
</div>