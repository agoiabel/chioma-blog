<!DOCTYPE html>
<!--[if IE 7 ]>    
<html class="ie7">
   <![endif]-->
<!--[if IE 8 ]>    
   <html class="ie8">
      <![endif]-->
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <!-- Mobile Specific Metas================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="keywords" content="{{$seo_helper_content}}"/>

    <link rel="shortcut icon" href="img/favicon.png" />
    <title>the moral woman</title>
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

	<link rel="stylesheet" href="{{asset('css/front-style.css')}}">
	@yield('header')
</head>

<body class="home">

    @include('shared._mobile_css')

    <div class="theme-layout">
        <!-- Start Header -->

        @include('shared._header')

		@yield('content')

		@include('shared._footer')        
    </div>
	<script src="{{asset('js/front-script.js')}}"></script>
    @include('shared.flash')
    
    @yield('footer')


</body>

</html>