<div class="page-sidebar sidebar">
    <div class="page-sidebar-inner slimscroll">
        
        <div class="sidebar-header">
            <div class="sidebar-profile">
                <a href="javascript:void(0);" id="profile-menu-link">
                    <div class="sidebar-profile-image">
                        <img src="{{asset(Auth::user()->picture)}}" class="img-circle img-responsive" alt="">
                    </div>
                    <div class="sidebar-profile-details">
                        <span>{{Auth::user()->name}}<br><small>{{Auth::user()->role->name}}</small></span>
                    </div>
                </a>
            </div>
        </div>

        <ul class="menu accordion-menu">
            <li class="{{isActiveRoute('dashboard')}}"><a href="{{route('dashboard')}}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>Dashboard</p></a></li>
            <li class="{{isActiveRoute('category.index')}}"><a href="{{route('category.index')}}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-th"></span><p>Category</p></a></li>
            
            <li class="{{isActiveRoute('blog.index')}}"><a href="{{route('blog.index')}}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-gift"></span><p>Blog</p></a></li>

            <li class="{{isActiveRoute('comment.index')}}"><a href="{{route('comment.index')}}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-comment"></span><p>Comment</p></a></li>
        </ul>
    </div><!-- Page Sidebar Inner -->
</div><!-- Page Sidebar -->
