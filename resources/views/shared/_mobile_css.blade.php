<div class="tw-mobile-menu right-side">
    <i class="ion-ios-close-empty"></i>
    <nav>
        <ul class="sf-mobile-menu clearfix">
            <li class="menu-item current-menu-item">
                <a href="{{route('index')}}">Home</a>
            </li>
            <li class="menu-item menu-item-has-children">
                <a href="#">Categories</a>
                <ul class="sub-menu">
                    @foreach ($categories as $category)
                        <li class="menu-item"><a href="{{route('category.show', $category->slug)}}">{{$category->name}}</a></li>
                    @endforeach
                </ul>
            </li>
        </ul>
    </nav>
</div>