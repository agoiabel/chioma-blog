

        <footer class="footer-area">
            <div class="footer-socials">
                <div class="container">
                    <div class="entry-share clearfix">
                        <div class="social-item"><a class="tw-meta" href="http://{{getSetting('facebook')}}"><i class="ion-social-facebook"></i><span>Facebook</span><span>Like</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://{{getSetting('twitter')}}"><i class="ion-social-twitter"></i><span>Twitter</span><span>Follow</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://{{getSetting('instagram')}}"><i class="ion-social-instagram"></i><span>Instagram</span><span>Follow</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://pinterest.com"><i class="ion-social-pinterest"></i><span>Pinterest</span><span>Follow</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://plus.google.com"><i class="ion-social-googleplus"></i><span>Google +</span><span>Follow</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://youtube.com"><i class="ion-social-youtube"></i><span>Youtube</span><span>Subscribe</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Container -->
            <div class="container">
                <div class="tw-footer clearfix">
                    <p class="copyright">Agoi Abel Adeyemi</a>
                    </p>
                    <p class="footer-text">© 2017 - All Rights Reserved</p>
                </div>
            </div>
            <!-- End Container -->
        </footer>