<header class="header-area header-2">
    <div class="tw-menu-container">
        <div class="container">
            <div class="tw-logo">
                <a class="logo" href="{{route('index')}}">
                    <img class="logo-img" src="{{asset('img/chioma_logo_home.png')}}" alt="Lovely - Elegant &amp; Simple Blog Theme" />
                </a>
            </div>
            <a href="{{route('index')}}" class="mobile-menu-icon">
                <span></span>
            </a>
            <nav class="tw-menu">
                <ul class="sf-menu">
                    <li class="menu-item current-menu-item">
                        <a href="{{route('index')}}">Home</a>
                    </li>
                    <li class="menu-item">
                        <a href="#">Categories</a>
                        <ul class="sub-menu">
                            @foreach ($categories as $category)
                                <li class="menu-item"><a href="{{route('category.show', $category->slug)}}">{{$category->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>

                </ul>
                <div class="social-icons">
                    <a title="Facebook" href="http://{{getSetting('facebook')}}" target="_blank"><i class="ion-social-facebook"></i></a>
                    <a title="Twitter" href="http://{{getSetting('twitter')}}" target="_blank"><i class="ion-social-twitter"></i></a>
                    <a title="Instagram" href="http://{{getSetting('instagram')}}" target="_blank"><i class="ion-social-instagram"></i></a>
                    <a title="Pinterest" href="http://pinterest.com"><i class="ion-social-pinterest"></i></a>
                    <a title="Google +" href="http://plus.google.com"><i class="ion-social-googleplus"></i></a>
                    <a title="Youtube" href="http://youtube.com"><i class="ion-social-youtube"></i></a>
                </div>
                <form method="get" class="searchform on-menu" action="#">
                    <div class="input">
                        <input type="text" value="" name="s" placeholder="Search" /><i class="ion-search"></i>
                    </div>
                </form>
            </nav>
        </div>
    </div>
    <div class="header-clone"></div>
</header>