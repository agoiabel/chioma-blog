<div class="navbar">
    <div class="navbar-inner">
        <div class="sidebar-pusher">
            <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="logo-box">
            <a href="{{route('dashboard')}}" class="logo-text"><span>Moral</span></a>
        </div><!-- Logo Box -->

        <div class="search-button">
            <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
        </div>

        <div class="topmenu-outer">
            <div class="top-menu">
                <ul class="nav navbar-nav navbar-left">
                    <li>        
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                            @if ( count($comments) > 0)
                                <i class="fa fa-comment"></i>
                                <span class="badge badge-success pull-right">
                                    {{ count($comments) }}
                                </span>
                            @endif
                        </a>
                        @if ( count($comments) > 0 )
                            <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                <li><p class="drop-title">You have {{count($comments)}} new  messages !</p></li>
                                <li class="dropdown-menu-list slimscroll messages">
                                    <ul class="list-unstyled">

                                        @foreach ($comments as $comment)
                                            <li>
                                                <a href="{{route('comment.index')}}">
                                                    <div class="msg-img"><div class="online on"></div><img class="img-circle" src="{{asset($comment->composer->picture)}}" alt=""></div>
                                                    <p class="msg-name">{{$comment->composer->name}}</p>
                                                    <p class="msg-text">{{words($comment->comment, 40)}}..</p>
                                                    <p class="msg-time">{{$comment->created_at->diffForHumans()}}</p>
                                                </a>
                                            </li>
                                        @endforeach

                                    </ul>
                                </li>
                                <li class="drop-all"><a href="{{route('comment.index')}}" class="text-center">All Comments</a></li>
                            </ul>
                        @endif
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                            <span class="user-name">{{Auth::user()->name}}<i class="fa fa-angle-down"></i></span>
                            <img class="img-circle avatar" src="{{asset(Auth::user()->picture)}}" width="40" height="40" alt="">
                        </a>
                        <ul class="dropdown-menu dropdown-list" role="menu">
                            <li role="presentation"><a href="{{route('setting')}}"><i class="fa fa-user"></i>Settings</a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a href="{{route('logout')}}"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('logout')}}" class="log-out waves-effect waves-button waves-classic">
                            <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                        </a>
                    </li>

                </ul><!-- Nav -->
            </div><!-- Top Menu -->
        </div>
    </div>
</div><!-- Navbar -->
