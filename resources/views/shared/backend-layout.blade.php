<!DOCTYPE html>
<html>
    
<head>
        
        <!-- Title -->
        <title>The Moral Woman | Blog</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="author" content="Agoi Abel" />
        <meta name="_token" content="{{ csrf_token() }}"/>
        
        <link rel="stylesheet" href="{{asset('/css/backend-style.css')}}">
        @yield('header')
        
        <script src="{{asset('assets/plugins/3d-bold-navigation/js/modernizr.js')}}"></script>
        <script src="{{asset('assets/plugins/offcanvasmenueffects/js/snap.svg-min.js')}}"></script>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="page-header-fixed">

        <main class="page-content content-wrap">

            @include('shared._backend_header_nav')

            @include('shared._backend_sidebar')

            <div class="page-inner">
                
                @include('shared._page_title')                

                @yield('content')
                
                @include('shared._backend_footer')                
            </div><!-- Page Inner -->
        </main><!-- Page Content -->


        <div class="cd-overlay"></div>
	

        <!-- <script src="{{asset('js/backend-script.js')}}"></script> -->

        <!-- Javascripts -->
        <script src="{{asset('assets/plugins/jquery/jquery-2.1.4.min.js')}}"></script>
        <script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/plugins/pace-master/pace.min.js')}}"></script>
        <script src="{{asset('assets/plugins/jquery-blockui/jquery.blockui.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assets/plugins/uniform/jquery.uniform.min.js')}}"></script>
        <script src="{{asset('assets/plugins/offcanvasmenueffects/js/classie.js')}}"></script>
        <script src="{{asset('assets/plugins/offcanvasmenueffects/js/main.js')}}"></script>
        <script src="{{asset('assets/plugins/waves/waves.min.js')}}"></script>

        <script src="{{asset('assets/plugins/3d-bold-navigation/js/main.js')}}"></script>
        <script src="{{asset('assets/plugins/waypoints/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('assets/plugins/jquery-counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('assets/plugins/toastr/toastr.min.js')}}"></script>
        <script src="{asset('assets/plugins/flot/jquery.flot.min.js')}"></script>
        <script src="{{asset('assets/plugins/flot/jquery.flot.time.min.js')}}"></script>
        <script src="{{asset('assets/plugins/flot/jquery.flot.symbol.min.js')}}"></script>
        <script src="{{asset('assets/plugins/flot/jquery.flot.resize.min.js')}}"></script>

        <script src="{{asset('assets/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{asset('assets/plugins/curvedlines/curvedLines.js')}}"></script>
        <script src="{{asset('assets/plugins/metrojs/MetroJs.min.js')}}"></script>
        <script src="{{asset('assets/js/modern.js')}}"></script>
        <script src="{{asset('js/libs/sweetalert.min.js')}}"></script>
        <!-- <script src="assets/js/pages/dashboard.js"></script> -->
        
        @yield('footer')        
        @include('shared.flash')
    </body>

</html>