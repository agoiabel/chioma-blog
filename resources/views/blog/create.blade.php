@extends('shared.backend-layout')
@section('header')
  <link href="{{asset('assets/plugins/summernote-master/summernote.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                
                <div class="panel-body">
  
                      <div class="btn-group pull-right">
                                  <a href="{{route('blog.index')}}" class="btn btn-success btn-rounded waves-effect waves-light"><span class="m-l-5">
                                  <i class="fa fa-thumbs-up"></i> </span>All Blogs </a>
                      </div>
                      <div class="c-spacer-10"></div>
                      @include('errors.list')
                      <form action="{{route('blog.store')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                          <div class="form-group">
                            <input type="text" name="title" value="{{old('title')}}" class="form-control" aria-required="true" aria-invalid="false" placeholder="blog title *">
                          </div>

                          <div class="form-group">
                            <select name="category_id" id="" class="form-control">
                                <option value="" disabled selected>Select blog category</option>
                                @foreach ($categories as $category)
                                  <option value="{{$category->id}}">{{$category->name}}</option>                          
                                @endforeach
                            </select>
                          </div>

                          <div class="form-group">
                            <input type="text" name="seo_helper_content" value="{{old('seo_helper_content')}}" class="form-control" aria-required="true" aria-invalid="false" 
                                   placeholder="google search keyword (seo)">
                          </div>

                          <div class="form-group">
                           {!! Form::textarea('blog_post', NULL, ['placeholder'=>'Write your comment','rows'=>3,'class'=>'form-control summernote']) !!}
                          </div>

                          <button type="submit" class="btn btn-success btn-block btn-rounded btn-lg">Submit</button>
                      </form>

                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
  <!-- <script src="{{asset('/js/libs/jquery.min.js')}}"></script> -->
<!--   <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
 -->
  <script src="{{asset('assets/plugins/summernote-master/summernote.min.js')}}"></script>  
  <script>
        // $('.description').ckeditor();

        $(document).ready(function () {

          $('.summernote').summernote({
            height: 350,
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
          });

          function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("_token", $("meta[name='_token']").attr('content'));
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('blog.content-image-upload')}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    console.log(url);
                    editor.insertImage(welEditable, url);
                }
            });
          }

        });

  </script>
@endsection