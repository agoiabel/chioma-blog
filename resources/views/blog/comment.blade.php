@extends('shared.backend-layout')
@section('header')
  <link rel="stylesheet" href="{{asset('/css/toastr.min.css')}}">
@endsection
@section('content')

<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">
                  
                    <div class="c-spacer-10"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Time</th>
                                    <th>Blog Title</th>
                                    <th>Blog Category</th>
                                    <th>Blog Comment</th>
                                    <th>Comment View Status</th>
                                    
                                    <th>Reply</th>
                                    <th>delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($comments as $comment)
                                <tr>    
                                    <td>{{ $comment->created_at->diffForHumans() }}</td>
                                    <td>{{ $comment->blogpost->title }}</td>
                                    <td>{{ $comment->blogpost->category->name }}</td>
                                    <td>{!! $comment->comment !!}</td>
                                    <td>{{ comment_read_status($comment->user_status) }}</td>
                                    <td>
                                      <button class="btn btn-info btn-sm reply" data-blog-post-id="{{$comment->blog_post_id}}" data-comment-id="{{$comment->id}}"><i class="fa fa-reply"></i></button>
                                    </td>
                                    <td>
                                      <a href="{{route('comment.delete', $comment->id)}}" class="btn btn-danger btn-sm delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>                        
                    </div>

                    {!! $comments->render() !!}
                </div>
            </div>
        </div>
    </div>

</div>

@include('blog._reply_comment')

@endsection
@section('footer')
  <script src="{{asset('/js/libs/toastr.min.js')}}"></script>
  <script>
    $(document).ready(function () {
      
      $(".reply").on('click', function (e) {
          e.preventDefault();

          var vm = $(this);
          var blog_post_id = vm.data('blog-post-id');
          var comment_id = vm.data('comment-id');
          var form = $("#reply_comment_form");
          $("#blog_post_id").val(blog_post_id);
          $("#comment_id").val(comment_id);

          $.ajax({
            type: "GET",
            url: '/comment/show/'+comment_id,
            success: function (data) {
              console.log(data);
              $("#comment_content").html(data.comment);
              $("#myModalLabel").html(data.blogpost.title);
              $("#myModal").modal('show');
            }
          });
    
          $("#submit_reply").on('click', function () {
              $.ajax({
                type: "POST",
                url: form.prop('action'),
                data: form.serialize(),   
                success: function (data) {
                  $("#comment").val("");
                  $("#myModal").modal('hide');
                  toastr.success(data);
                }
              });
          });
      });

    });
  </script>
@endsection