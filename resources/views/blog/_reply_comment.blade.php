

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">           
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div id="comment_content"></div><hr>
                <div class="c-spacer-10"></div>

                <form action="{{route('comment.store')}}" method="POST" id="reply_comment_form">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">  
                    <input type="hidden" name="email" value="{!! Auth::user()->email !!}">  
                    <input type="hidden" name="name" value="{!! Auth::user()->name !!}">
                    <input type="hidden" name="blog_post_id" value="" id="blog_post_id">
                    <input type="hidden" name="comment_id" value="" id="comment_id">
                    <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit_reply">Reply</button>
            </div>


        </div>
    </div>
</div>