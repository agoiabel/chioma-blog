@extends('shared.backend-layout')
@section('header')
  <!-- <link rel="stylesheet" href="{{asset('/css/dropzone.css')}}"> -->
  <link rel="stylesheet" href="{{asset('/css/jquery.filer.css')}}">
@endsection
@section('content')

<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                
                <div class="panel-body">
  
                      <div class="btn-group pull-right">
                                  <a href="{{route('blog.index')}}" class="btn btn-success btn-rounded waves-effect waves-light"><span class="m-l-5">
                                  <i class="fa fa-thumbs-up"></i> </span>All Blogs </a>
                      </div>
                      <div class="c-spacer-10"></div>
                        
                        {!! Form::open(['route' => ['blog.post-image'], 'class' => 'form-horizontal' , 'files' => true]) !!}

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                        <input type="hidden" name="blog_post_id" value="{{$blog->id}}">

                        
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="file" name="photo" id="filer_input" class="form-control">
                          </div>
                        </div>
                        
                        <div class="col-md-3">
                          <div class="form-group">
                              <input type="text" name="caption" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="Image Caption *">
                          </div>
                        </div>

                        <div class="col-md-3">
                            <div class="">                            
                              <select name="upload_another" id="" class="form-control">
                                  <option value="" disabled selected>upload another image ?</option>
                                  <option value="yes">yes</option>
                                  <option value="no">no</option>
                              </select>
                            </div>
                        </div>


                        <div class="row col-md-12">
                          <button type="submit" class="btn btn-success">submit</button>
                        </div>


                      </form>

                </div>

            </div>
        </div>

        <div class="col-md-12">
          <div class="panel panel-white">
            <div class="panel-body">
                    
            </div>
          </div>
        </div>
        
    </div>
</div>



@endsection

@section('footer')
  <script src="{{asset('/js/libs/jquery.filer.js')}}"></script>
  <script>
    $(document).ready(function() {
      $('#filer_input').filer({
        showThumbs: true,
      });
    });
  </script>
@endsection