@extends('shared.backend-layout')
@section('content')

<div id="main-wrapper" ng-app="blogApp" ng-controller="blogCtrl">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">

                    <div class="btn-group pull-right">
                              <a href="{{route('blog.create')}}" class="btn btn-success btn-rounded waves-effect waves-light"><span class="m-l-5">
                              <i class="fa fa-plus"></i> </span>Add New </a>
                    </div>
                  
                    <div class="c-spacer-10"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Blog Category</th>
                                    <th>Blog Post</th>
                                    <th>no of view(s)</th>
                                    <th>no of like(s)</th>
                                    <th>no of comment(s)</th>
                                    <th>edit</th>
                                    <th>delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="blog in blogs track by $index">
                                    <td>@{{ blog.title }}</td>
                                    <td>@{{ blog.category.name }}</td>
                                    <td ng-bind-html="blog.blog_post | limitTo: 200"></td>
                                    <td>@{{ blog.pagestat.no_of_view }}</td>
                                    <td>@{{ blog.numberOfLike }}</td>
                                    <td>@{{ blog.comments.length }}</td>
                                    <td>
                                      <a href="javascript:" class="btn btn-info btn-sm" ng-click="editBlog(blog.slug)"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td>
                                      <a href="javascript:" class="btn btn-danger btn-sm" ng-click="deleteBlog(blog, $index)"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>                        
                    </div>

                    <div ng-click="loadMore()" align="center">
                        <button class="submit btn btn-success btn-rounded">
                            <div ng-if="loading"><span>loading...</span></div>
                            <div ng-if="!loading"><span>Load More <i class="ion-ios-arrow-thin-right"></i></span></div>
                        </button>               
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('footer')
<script src="{{asset('/js/libs/angular.js')}}"></script>
<script src="{{asset('/js/libs/angular-sanitize.js')}}"></script>

<script>
    var blogApp = angular.module('blogApp', ['ngSanitize']);

    blogApp.controller('blogCtrl', function ($scope, $http, $window) {
        $scope.blogs = [];
        $scope.loading = false;

        $scope.init = function () {
            $scope.lastpage = 1;
            $http({
                url: '/api/blog/list?page='+$scope.lastpage,
                method: "GET",
            }).success(function (data) {                    
                console.log(data.data);
                $scope.blogs = data.data;
                $scope.currentpage = data.current_page;
            });

            $scope.loadMore = function () {
                $scope.loading = true;          
                $scope.loadMoreBlog($scope.lastpage + 1);
            }

        }

        $scope.init();

        $scope.loadMoreBlog = function (page) {
            $http({
                url: '/api/blog/list?page='+page,
                method: "GET",
            }).success(function (data) {
                $scope.loading = false;
                $scope.blogs = $scope.blogs.concat(data.data);
                $scope.currentpage = data.current_page;
            });
        }
        
        $scope.deleteBlog = function (blog, key) {
            if ( confirm("Are you sure") ) {
                $scope.blogs.splice(key, 1); 

                $http({
                    url: '/api/blog/destroy/'+blog.id,
                    method: "GET",
                }).success(function (data) {
                    alert('you successfully deleted the blog post');
                });

            }
        }

        $scope.editBlog = function (slug) {
            console.log(slug);
            $window.location.href = "/blog/edit/"+slug;
        }

    });
</script>

@endsection
