@extends('shared.backend-layout')
@section('content')

<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                      
                <div class="panel-body">
                  
                      <div class="btn-group pull-right">
                                  <a href="{{route('category.index')}}" class="btn btn-success btn-rounded waves-effect waves-light"><span class="m-l-5">
                                  <i class="fa fa-thumbs-up"></i> </span>All Category </a>
                      </div>
                      <div class="c-spacer-10"></div>
                      @include('errors.list')
                      <form action="{{route('category.store')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                          <div class="form-group">
                            <input type="text" name="name" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="category name *">
                          </div>

                          <button type="submit" class="btn btn-success btn-block btn-rounded btn-lg">Submit</button>
                      </form>

                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
<script src="{{asset('/js/libs/angular.js')}}"></script>

@endsection