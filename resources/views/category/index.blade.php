@extends('shared.backend-layout')
@section('content')
<div id="main-wrapper" ng-app="categoryApp" ng-controller="categoryCtrl">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">

                  <div class="btn-group pull-right">
                              <a href="{{route('category.create')}}" class="btn btn-success btn-rounded waves-effect waves-light"><span class="m-l-5">
                              <i class="fa fa-plus"></i> </span>Add New </a>
                  </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="category in categories track by $index">
                                <td>@{{ category.id }}</t>
                                <td>@{{ category.name }}</td>
                                <td><a href="javascript:" class="btn btn-danger" ng-click="deleteCategory(category, $index)"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div align="center" ng-if="loading">
        <button class="submit">..loading</button>               
    </div>
</div>
@endsection

@section('footer')
<script src="{{asset('/js/libs/angular.js')}}"></script>
<script>
    var categoryApp = angular.module('categoryApp', []);

    categoryApp.controller('categoryCtrl', function ($scope, $http, $window) {
        $scope.categories = [];
        $scope.loading = false;

        $scope.init = function () {
            $scope.lastpage = 1;
            $http({
                url: '/api/category/list?page='+$scope.lastpage,
                method: "GET",
            }).success(function (data) {                    
                console.log(data.data);
                $scope.categories = data.data;
                $scope.currentpage = data.current_page;
            });

            $window.onscroll = function () {
                if (document.body.scrollHeight == document.body.scrollTop + window.innerHeight) {                    
                    $scope.loading = true;          
                    $scope.lastpage = $scope.lastpage + 1;
                    $scope.loadMoreCategory($scope.lastpage);
                }
            }

        }

        $scope.init();

        $scope.loadMoreCategory = function (page) {
            $http({
                url: '/api/category/list?page='+page,
                method: "GET",
            }).success(function (data) {
                $scope.loading = false;
                $scope.blogs = $scope.categories.concat(data.data);
                $scope.currentpage = data.current_page;
            });
        }
        
        $scope.deleteCategory = function (category, key) {
            if ( confirm("Are you sure") ) {
                $scope.categories.splice(key, 1); 

                $http({
                    url: '/api/category/destroy/'+category.id,
                    method: "GET",
                }).success(function (data) {
                    alert('you successfully deleted the blog post');
                });

            }
        }

        $scope.showCategory = function (slug) {
            console.log(slug);
            // $window.location.href = "/single-blog/"+slug;
        }

    });

</script>
@endsection