@extends('shared.single-layout', ['seo_helper_content' => $blog->seo_helper_content])
@section('content')
    <div class="lovely-container container">
        <div class="row">
            <div class="content-area with-sidebar col-md-8">
                <article class="single">
                    <div class="entry-cats"><a href="{{route('category.show', $blog->category->slug)}}" title="View all posts in Music">{{$blog->category->name}}</a><span>, </span>
                    </div>
                    <h1 class="entry-title">{{$blog->title}}</h1>
                    <div class="entry-date tw-meta">{{$blog->created_at->diffForHumans()}} / <span class="entry-author">By&nbsp;<a href="javascript:" title="Posts by Themewaves" rel="author">{{$blog->user->name}}</a></span>
                    </div>
                    <div class="entry-media">
                        <div class="owl-carousel">
                            @foreach ($blog->images as $image)
                                <div class="feature-item tw-middle" style="background-image:url({{asset($image->full_image)}});">
                                </div>
                            @endforeach
                        </div>
                    </div> 
                    <div class="entry-content">
                        {!! $blog->blog_post !!}
                    </div>

                    <div class="entry-share clearfix">
                        <a href="{{route('blog.update-like', $blog->id)}}" title="Share this"><i class="ion-thumbsup"></i><span>{{$blog->numberOfLike}}</span></a>

                        <a href="javascript:" title="no comment" class="comment-count"><i class="ion-chatbubbles"></i><span>{{$blog->pagestat->no_of_view}} view (s)</span></a>
                    </div>

                </article>
                
                <div class="nextprev-postlink">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="prev-post-link">
                                @if (!is_null($previous_slug))
                                <a href="{{route('single-blog', $previous_slug)}}" title="Cup of Coffee">
                                    <span class="tw-meta"><i class="ion-ios-arrow-thin-left"></i>Previous Article</span>
                                </a>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="next-post-link">
                                @if (!is_null($next_slug))
                                <a href="{{route('single-blog', $next_slug)}}">
                                    <span class="tw-meta">Next Article<i class="ion-ios-arrow-thin-right"></i></span>
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="tw-author">
                    <div class="author-box">
                        <img src="{{asset($blog->user->picture)}}" width="120" height="120" alt="" class="avatar" />
                        <h3><a href="#" title="{{$blog->user->name}}" rel="author">{{$blog->user->name}}</a></h3>
                        <span class="tw-meta">writer</span>
                        <p>
                            {!! $blog->user->description !!}
                        </p>
                        <div class="entry-share">
                            <a href="http://{{getSetting('facebook')}}" target="_blank"><i class="ion-social-facebook"></i></a>
                            <a href="http://{{getSetting('twitter')}}" target="_blank"><i class="ion-social-twitter"></i></a>
                            <a href="http://{{getSetting('instagram')}}" target="_blank"><i class="ion-social-instagram"></i></a>
                        </div>
                    </div>
                </div>
                

                <div class="entry-comments" id="comments">
                        <div class="comment-title">
                            <h4 class="np-title-line">
                                @if (! $blog->comments->isEmpty())
                                    {{$blog->comments->count()}} Comment                        
                                @endif
                              </h4>
                        </div>
                        <div class="comment-list clearfix">

                            @foreach ($blog->comments as $comment)
                                <div class="comment even thread-even depth-1" id="comment-6">
                                    <div class="comment-author">
                                        <img src="{{asset($comment->composer->picture)}}" width="60" height="60" alt="" class="avatar avatar-60wp-user-avatar wp-user-avatar-60 alignnone photo avatar-default">
                                    </div>
                                    <div class="comment-text">
                                        <h3 class="author"><a href="#" rel="external nofollow" class="url">{{$comment->composer->name}}</a></h3>
                                        <span class="entry-date tw-meta">{{$comment->created_at}}</span>
                                        <p>
                                            {!! $comment->comment !!}
                                        </p>
                                    </div>
                                </div>                        
                            @endforeach

                        </div>
                        <div class="navigation">
                            <div class="left"></div>
                            <div class="right"></div>
                        </div>
                        <div id="respond" class="comment-respond">
                            <h3 id="reply-title" class="comment-reply-title">Leave a Comment</h3>
                            <form action="{{route('comment.store')}}" method="post" id="commentform" class="comment-form">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                <p class="comment-form-comment">
                                    <textarea name="comment" placeholder="Your message" id="comment" class="required" rows="7" tabindex="4"></textarea>
                                </p>
                                <p class="comment-form-author">
                                    <input id="author" name="name" placeholder="Name *" type="text" value="" size="30" aria-required="true">
                                </p>
                                <p class="comment-form-email">
                                    <input id="email" name="email" placeholder="Email *" type="text" value="" size="30" aria-required="true">
                                </p>

                                <div class="clearfix"></div>
                                <p class="form-submit">
                                    <input type="hidden" name="blog_post_id" value="{{$blog->id}}" id="comment_post_ID">
                                    <input name="submit" type="submit" id="submit" class="submit" value="Post comment">
                                </p>
                            </form>
                        </div>
                        <!-- #respond -->
                    </div>

            </div>
            
            @include('guest._sidebar')

        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('/js/libs/angular.js')}}"></script>
    <script src="{{asset('/js/libs/angular-sanitize.js')}}"></script>

    @include('shared.flash')

    <script>
        blogApp = angular.module('blogApp', ['ngSanitize']);

        blogApp.controller('blogCtrl', function ($scope, $http, $window) {

            $scope.init = function () {

            }

            $scope.init();

            $scope.likeBlog = function (blog) {
                console.log(blog);
                blog.numberOfLike = parseInt(blog.numberOfLike);
                blog.numberOfLike = blog.numberOfLike + 1;
                $http({
                    url: '/api/blog/update-like/'+blog.id,
                    method: "GET",
                }).success(function (data) {
                    // toastr.success(data);
                });
            }

        });

    </script>

@endsection