<div class="feature-area">
    <div class="container">
        <div>
            <div class="owl-carousel">

                @foreach ($blogs as $blog)
                    @foreach ($blog->images as $image)
                        <div class="feature-item tw-middle" style="background-image:url({{$image->full_image}});">
                            <div class="feature-content">
                                <div class="entry-content">
                                    <div class="entry-cats"><a href="{{route('category.show', $blog->category->slug)}}" title="View all posts in Fashion" >{{$blog->category->name}}</a><span>, </span></div>
                                    <h2 class="entry-title"><a href="{{route('single-blog', $blog->slug)}}">{{ words($blog->title, 10) }}</a></h2>
                                    <div class="entry-date tw-meta">
                                        <span>{{$blog->created_at->diffForHumans()}}</span> / <span class="entry-author">By<a href="#" title="Posts by {{$blog->user->name}}" rel="author"> {{$blog->user->name}}</a></span>
                                    </div>
                                    <div class="more-link tw-hover tw-meta"><a href="{{route('single-blog', $blog->slug)}}"><span>Read more</span><i class="ion-ios-arrow-thin-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach

            </div>  
        </div>
    </div>
</div>