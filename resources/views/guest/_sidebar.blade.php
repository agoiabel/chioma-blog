<div class="sidebar-area col-md-4">
    <div class="sidebar-inner clearfix">
        <div class="widget-item">
            <aside class="widget socialtabswidget" id="lovely_social_tabs_widget-2">
                <h3 class="widget-title"><span>Follow Us</span></h3>
                <div class="lovely-social-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs entry-share tab-count-4" role="tablist">
                        <li role="presentation"><a href="" role="tab" data-toggle="tab"><i class="ion-social-facebook"></i></a></li>
                        <li role="presentation"><a href="#" role="tab" data-toggle="tab"><i class="ion-social-pinterest"></i></a></li>
                        <li role="presentation"><a href="#" role="tab" data-toggle="tab"><i class="ion-social-instagram"></i></a></li>
                        <li role="presentation"><a href="#" role="tab" data-toggle="tab"><i class="ion-social-twitter"></i></a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane">
                            <a href="http://{{getSetting('facebook')}}" target="_blank"><img src="{{asset('img/tab-facebook.png')}}" alt="Share" /></a>
                        </div>
                        <div role="tabpanel" class="tab-pane">
                            <a href="http://{{getSetting('facebook')}}" target="_blank"><img src="{{asset('img/tab-pinterest.png')}}" alt="Pin"/></a>
                        </div>
                        <div role="tabpanel" class="tab-pane">
                            <a href="http://{{getSetting('instagram')}}" target="_blank"><img src="{{asset('img/tab-instagram.png')}}" alt="Share" /></a>
                        </div>
                        <div role="tabpanel" class="tab-pane">
                            <a href="http://{{getSetting('twitter')}}" target="_blank"><img src="{{asset('img/tab-twitter.png')}}" alt="Tweet" /></a>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
        <div class="widget-item">
            <aside class="widget widget_categories" id="categories-2">
                <h3 class="widget-title"><span>Category</span></h3>
                <ul>
                    @foreach ($categories as $category)
                        <li class="cat-item cat-item-2"><a href="{{route('category.show', $category->slug)}}">{{$category->name}}</a> ({{$category->blogposts->count()}})</li>
                    @endforeach
                </ul>
            </aside>
        </div>
        <div class="widget-item">
            <aside class="widget widget_subscribe">
                <h3 class="widget-title"><span>Newsletter</span></h3>
                <div class="widget_subscribe_cont">
                    <form method="post" action="#" class="widget_subscribe">
                        <p class="subscribe-paragraph">
                            <input type="text" name="your_email" title="Your Email" placeholder="Your Email" value="" />
                            <span class="abs-req">
                            <input type="text" name="input_name" value="" />
                          </span>
                        </p>
                        Subscribe to my Newsletter
                        <input class="subscribe-submit" type="submit" value="Subscribe" />
                    </form>
                </div>
            </aside>
        </div>
    </div>
</div>