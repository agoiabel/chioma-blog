@extends('shared.layout')
@section('content')
        <div class="feature-area">
            <div class="container">
                <div class="feature-title">
                    <span class="tw-meta">Browsing category</span>
                    <h1>{{$category->name}}</h1>
                </div>
            </div>
        </div>
        <div class="lovely-container container">
            <div class="row">
                <div class="content-area list-side col-md-8">
                    <div class="list-blog" ng-app="categoryApp" ng-controller="categoryCtrl">

                        <article class="hentry" ng-repeat="blog in blogs">
                            <div class="entry-post">
                                <div class="entry-media">
                                    <div class="tw-thumbnail"><img width="220" height="200" src="{!! asset('@{{blog.images[0].image}}') !!}" alt="" />
                                        <div class="image-overlay tw-middle">
                                            <div class="image-overlay-inner">
                                                <a href="javascript:" ng-click="loadBlog(blog.slug)" title="Beautiful Girl" class="overlay-icon"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-cats"><a href="javascript:" ng-click="loadCategoryBlogFor(blog.category.slug)" title="View all posts in Fashion">@{{blog.category.name}}</a><span>, </span>
                                </div>
                                <h2 class="entry-title"><a href="javascript:" ng-click="loadBlog(blog.slug)">@{{blog.title | limitTo: 30}} @{{blog.title.length > 30 ? '...' : ''}}</a></h2>
                                <div class="entry-content clearfix">
                                    <!-- <p ng-bind-html="blog.blog_post | limitTo: 100 blog.blog_post.length > 80 ? '...' : ''"></p> -->
                                    <p ng-bind-html="blog.blog_post | limitTo: 200"></p>

                                    <!-- <p>@{{ blog.blog_post | limitTo: 80 }} @{{blog.blog_post.length > 80 ? '...' : ''}}</p> -->
                                    <div class="entry-date tw-meta">@{{ blog.created_at | date:"fullDate" }}</div>
                                </div>
                            </div>
                        </article>
                    
                        <div align="center" ng-if="loading">
                            <button class="submit">loading</button>               
                        </div>

                    </div>
                    <div class="tw-pagination tw-hover tw-meta clearfix">
                        <div class="older"></div>
                        <div class="newer"></div>
                    </div>
                </div>
                
                @include('guest._sidebar')

            </div>
        </div>
@endsection

@section('footer')
<script src="{{asset('/js/libs/angular.js')}}"></script>
<script src="{{asset('/js/libs/angular-sanitize.js')}}"></script>

<script>
    var categoryApp = angular.module('categoryApp', ['ngSanitize']);

    categoryApp.controller('categoryCtrl', function ($scope, $http, $window) {
        $scope.blogs = [];
        $scope.loading = false;

        $scope.init = function () {
            $scope.lastpage = 1;
            $http({
                url: '/api/category/blog-list/{{$category->slug}}?page='+$scope.lastpage,
                method: "GET",
            }).success(function (data) {                    
                console.log(data.data);
                $scope.blogs = data.data;
                $scope.currentpage = data.current_page;
            });

            $window.onscroll = function () {
                if (document.body.scrollHeight == document.body.scrollTop + window.innerHeight) {
                    
                    console.log('bottom of page reached');
                    $scope.loading = true;          
                    $scope.lastpage = $scope.lastpage + 1;
                    $scope.loadMoreBlog($scope.lastpage);
                }
            }

        }

        $scope.init();

        $scope.loadMoreBlog = function (page) {
            $http({
                url: '/api/category/blog-list/{{$category->slug}}?page='+page,
                method: "GET",
            }).success(function (data) {
                $scope.loading = false;
                $scope.blogs = $scope.blogs.concat(data.data);
                $scope.currentpage = data.current_page;
            });
        }
        
        $scope.loadCategoryBlogFor = function (slug) {
            console.log(slug);
            $window.location.href = "/category/"+slug;
        }

        $scope.loadBlog = function (slug) {
            console.log(slug);
            $window.location.href = "/single-blog/"+slug;
        }

    });
</script>

@endsection