
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <!-- Mobile Specific Metas================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="img/favicon.png" />
    <title>the moral woman</title>

    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/front-style.css')}}" type="text/css">
	
</head>

<body>
    <div class="tw-mobile-menu"><i class="ion-ios-close-empty"></i>
        <nav>
            <ul class="sf-mobile-menu clearfix">
                <li class="menu-item current-menu-item">
                    <a href="{{route('index')}}">Home</a>
                </li>

                <li class="menu-item menu-item-has-children">
                        <a href="#">Categories</a>
                        <ul class="sub-menu">
                            @foreach ($categories as $category)
                                <li class="menu-item"><a href="{{route('category.show', $category->slug)}}">{{$category->name}}</a></li>
                            @endforeach
                        </ul>
                </li>

            </ul>
        </nav>
    </div>
    <div class="theme-layout">
        <!-- Start Header -->
        <header class="header-area">
            <div class="tw-menu-container">
                <div class="container">
                    <a href="#" class="mobile-menu-icon"><span></span></a>
                    <nav class="tw-menu">
                        <ul class="sf-menu">
                            <li class="menu-item current-menu-item">
                                <a href="{{route('index')}}">Home</a>
                            </li>
                            <li class="menu-item menu-item-has-children">
                                <a href="#">Categories</a>
                                <ul class="sub-menu">
                                    @foreach ($categories as $category)
                                        <li class="menu-item"><a href="{{route('category.show', $category->slug)}}">{{$category->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>

                        </ul>
                        <form method="get" class="searchform on-menu" action="#">
                            <div class="input">
                                <input type="text" value="" name="s" placeholder="Search" /><i class="ion-search"></i></div>
                        </form>
                    </nav>
                </div>
            </div>
            <div class="header-clone"></div>
            <div class="container">
                <div class="tw-logo">
                    <a class="logo" href="{{route('index')}}"><img class="logo-img" src="{{asset('img/chioma_logo.png')}}" alt="Lovely &amp; Elegant &amp; Simple Blog Theme" />
                    </a>
                </div>
            </div>
        </header>

        @include('guest._index_slider')

        <!-- Start Main -->
        <div class="lovely-container container" ng-app="blogApp" ng-controller="blogCtrl">
            <div class="row">
                <div class="content-area grid-side col-md-8">
                    <div class="grid-blog clearfix">

                        <article class="hentry" ng-repeat="blog in blogs">
                            <div class="entry-post">
                                <div class="entry-cats">
                                    <a href="javascript:" ng-click="loadBlog(blog.slug)" title="View all posts in Travel">@{{blog.title}}</a><span>, </span>
                                </div>
                                <h2 class="entry-title"><a href="javascript:" ng-click="loadCategoryBlogFor(blog.category.slug)">@{{blog.category.name}}</a></h2>
                                <div class="entry-date tw-meta">
                                    <span>@{{ blog.created_at | date:"fullDate" }}</span> / <span class="entry-author"></span>
                                </div>
                                <div class="entry-media">
                                    <div class="tw-thumbnail">
                                        <img width="620" height="350" src="{!! asset('@{{blog.images[0].image}}') !!}" alt="">
                                        <div class="image-overlay tw-middle">
                                            <div class="image-overlay-inner"><a href="#"  title="Morning Breakfast" class="overlay-icon"></a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-content clearfix">
                                    <!-- <p ng-bind-html="blog.blog_post | limitTo: 100 blog.blog_post.length > 80 ? '...' : ''"></p> -->
                                    <p ng-bind-html="blog.blog_post | limitTo: 200 "></p>
                                    <p class="more-link tw-hover tw-meta"><a href="javascript:" ng-click="loadBlog(blog.slug)"><span>Read more</span><i class="ion-ios-arrow-thin-right"></i></a></p>
                                </div>
                                <div class="entry-share">
                                    <a class="facebook-share" href="javascript:" title="Share this" ng-click="likeBlog(blog)"><i class="ion-thumbsup"></i><span>@{{blog.numberOfLike}}</span></a>
                                    <!-- <a href="javascript:" title="no comment" class="comment-count"><i class="ion-chatbubbles"></i><span>@{{blog.pagestat.length}} view (s)</span></a> -->
                                </div>
                            </div>
                        </article>
                    </div>

                    <div class="tw-pagination tw-hover tw-meta clearfix">
                        <div class="older">
                            <button ng-click="loadMore()">
                                <div ng-if="loading"><span>loading...</span></div>
                                <div ng-if="!loading"><span>Older Posts <i class="ion-ios-arrow-thin-right"></i></span></div>
                            </button>
                        </div>
                        <div class="newer"></div>
                    </div>
                </div>
                @include('guest._sidebar')
            </div>
            <div class="tw-popular-posts">
                <h3 class="widget-title">Popular posts</h3>
                <div class="row">

                    @foreach ($blogs as $blog)                
                        <div class="popular-item col-md-4">
                            <div class="popular-thumb"><img width="300" height="200" src="{{$blog->images[0]->image}}" alt="">
                                <div class="popular-content">
                                    <div class="entry-content">
                                        <h2 class="entry-title"><a href="{{route('single-blog', $blog->slug)}}">{{words($blog->title, 10)}}</a>
                                        </h2><span class="entry-date tw-meta">{{$blog->created_at->diffForHumans()}}</span><a href="{{route('single-blog', $blog->slug)}}"><i class="ion-android-add"></i></a></div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>


        <footer class="footer-area">
            <div class="footer-socials">
                <div class="container">
                    <div class="entry-share clearfix">
                        <div class="social-item"><a class="tw-meta" href="http://{{getSetting('facebook')}}"><i class="ion-social-facebook"></i><span>Facebook</span><span>Like</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://{{getSetting('twitter')}}"><i class="ion-social-twitter"></i><span>Twitter</span><span>Follow</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://{{getSetting('instagram')}}"><i class="ion-social-instagram"></i><span>Instagram</span><span>Follow</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://pinterest.com"><i class="ion-social-pinterest"></i><span>Pinterest</span><span>Follow</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://plus.google.com"><i class="ion-social-googleplus"></i><span>Google +</span><span>Follow</span></a>
                        </div>
                        <div class="social-item"><a class="tw-meta" href="http://youtube.com"><i class="ion-social-youtube"></i><span>Youtube</span><span>Subscribe</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Container -->
            <div class="container">
                <div class="tw-footer clearfix">
                    <p class="copyright">Agoi Abel Adeyemi</a>
                    </p>
                    <p class="footer-text">© 2017 - All Rights Reserved</p>
                </div>
            </div>
            <!-- End Container -->
        </footer>
    </div>
    <script src="{{asset('/js/front-script.js')}}"></script>
    <script src="{{asset('/js/libs/angular.js')}}"></script>
    <script src="{{asset('/js/libs/angular-sanitize.js')}}"></script>

    @include('shared.flash')

    <script>
        blogApp = angular.module('blogApp', ['ngSanitize']);

        blogApp.controller('blogCtrl', function ($scope, $http, $window) {
            $scope.blogs = [];
            $scope.loading = false;

            $scope.init = function () {
                $scope.lastpage = 1;
                $http({
                    url: '/api/blog/list?page='+$scope.lastpage,
                    method: "GET",
                }).success(function (data) {                    
                    $scope.blogs = data.data;
                    $scope.currentpage = data.current_page;
                });
            }

            $scope.init();

            $scope.loadMore = function () {
                $scope.loading = true;          
                $scope.loadMoreBlog($scope.lastpage + 1);
            }

            $scope.loadMoreBlog = function (page) {
                $http({
                    url: 'api/blog/list?page='+page,
                    method: "GET",
                }).success(function (data) {
                    $scope.loading = false;
                    $scope.blogs = $scope.blogs.concat(data.data);
                    $scope.currentpage = data.current_page;
                });
            }
            
            $scope.loadCategoryBlogFor = function (slug) {
                console.log(slug);
                $window.location.href = "/category/"+slug;
            }

            $scope.loadBlog = function (slug) {
                console.log(slug);
                $window.location.href = "/single-blog/"+slug;
            }

            $scope.likeBlog = function (blog) {
                console.log(blog);
                blog.numberOfLike = parseInt(blog.numberOfLike);
                blog.numberOfLike = blog.numberOfLike + 1;
                $http({
                    url: '/api/blog/update-like/'+blog.id,
                    method: "GET",
                }).success(function (data) {
                    // toastr.success(data);
                });
            }

        });

    </script>


</body>

</html>