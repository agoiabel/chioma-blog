@extends('shared.backend-layout')
@section('content')
<div class="profile-cover">
    <div class="row">
        <div class="col-md-3 profile-image">
            <div class="profile-image-container">
                <img src="{{asset(Auth::user()->picture)}}" alt="">
            </div>
        </div>
        <div class="col-md-12 profile-info">
            <div class=" profile-info-value">
                <h3>{{Auth::user()->blogposts->count()}}</h3>
                <p>Posts</p>
            </div>
            <div class=" profile-info-value">
                <h3>{{Auth::user()->composed_comments->count()}}</h3>
                <p>Comments</p>
            </div>
        </div>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-3 user-profile">
            <div class="panel panel-white">
                
                <div class="panel-heading">
                    <div class="panel-title">{{Auth::user()->name}}</div>
                </div>                
                @if (Auth::user()->isAdmin())
                <div class="panel-body">
					<form action="{{route('setting')}}" method="POST">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<div class="form-group">
							<input type="text" name="facebook" id="" placeholder="facebook" class="form-control" value={{getSetting('facebook')}}>
						</div>
						<div class="form-group">
							<input type="text" name="twitter" id="" placeholder="twitter" class="form-control" value="{{getSetting('twitter')}}">	
						</div>
						<div class="form-group">
							<input type="text" name="gmail" id="" placeholder="gmail" class="form-control" value="{{getSetting('gmail')}}">	
						</div>
						<div class="form-group">
							<input type="text" name="instagram" id="" placeholder="instagram" class="form-control" value="{{getSetting('instagram')}}">	
						</div>
						<button class="btn btn-primary btn-block">update</button>
					</form>
                </div>
                @endif


                
            </div>

        </div>
        <div class="col-md-6 m-t-lg">
            <div class="panel panel-white">
                
                <div class="panel-heading">
                    <div class="panel-title">Primary</div>
                </div>                
                <div class="panel-body">
					{!! Form::open(['route' => 'profile.update', 'files' => true]) !!}
						<input type="hidden" name="user_id" value="{{Auth::user()->id}}">

						<div class="form-group">
							<input type="text" name="name" id="" placeholder="name" class="form-control" value="{{Auth::user()->name}}">	
						</div>
						<div class="form-group">
							<input type="email" name="email" id="" placeholder="email" class="form-control" value="{{Auth::user()->email}}">	
						</div>
						<div class="form-group">
							<input type="file" name="picture" class="form-control" value="{{Auth::user()->picture}}">	
						</div>
						<div class="form-group">
							<textarea name="description" id="" cols="30" rows="5" class="form-control">{{Auth::user()->description}}</textarea>
						</div>

						<button class="btn btn-primary btn-block">update</button>
					</form>
                </div>

                
            </div>
        </div>
        <div class="col-md-3 m-t-lg">

            <div class="panel panel-white">
                <div class="panel-heading">
                    <div class="panel-title">Password Reset</div>
                </div>
                <div class="panel-body">
					<form action="{{route('password.reset')}}" method="POST">
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
						<div class="form-group">
							<input type="password" name="old_password" id="" placeholder="old_password" class="form-control">	
						</div>
						<div class="form-group">
							<input type="password" name="password" id="" placeholder="new password" class="form-control">	
						</div>
						<div class="form-group">
							<input type="password" name="password_confirmation" id="" placeholder="confirm password" class="form-control">	
						</div>

						<button class="btn btn-primary btn-block">update</button>
					</form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection